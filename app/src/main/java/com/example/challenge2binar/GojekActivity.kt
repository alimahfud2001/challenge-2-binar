package com.example.challenge2binar

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.challenge2binar.Adapter.MyAdapter
import com.example.challenge2binar.Data.MyData
import com.example.challenge2binar.databinding.ActivityGojekBinding

class GojekActivity : AppCompatActivity() {

    private lateinit var binding: ActivityGojekBinding
    var arrayList: ArrayList<MyData> = ArrayList()
    var myAdapter: MyAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityGojekBinding.inflate(layoutInflater)
        setContentView(binding.root)

        arrayList.add(MyData(R.drawable.ic_baseline_moped_24, "GoRide"))
        arrayList.add(MyData(R.drawable.ic_baseline_moped_24, "GoCar"))
        arrayList.add(MyData(R.drawable.ic_baseline_moped_24, "GoFood"))
        arrayList.add(MyData(R.drawable.ic_baseline_moped_24, "GoSend"))
        arrayList.add(MyData(R.drawable.ic_baseline_moped_24, "GoMart"))
        arrayList.add(MyData(R.drawable.ic_baseline_moped_24, "GoPulsa"))
        arrayList.add(MyData(R.drawable.ic_baseline_moped_24, "Check In"))
        arrayList.add(MyData(R.drawable.ic_baseline_moped_24, "More"))

        myAdapter = MyAdapter(this, arrayList)
        binding.gvData.adapter = myAdapter

//        binding.gvData.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
//            Toast.makeText(
//                applicationContext, "You CLicked " + arrayList[+position].name,
//                Toast.LENGTH_SHORT
//            ).show()
//        }
    }
}