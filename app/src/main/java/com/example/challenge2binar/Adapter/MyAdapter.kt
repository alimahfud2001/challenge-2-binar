package com.example.challenge2binar.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.example.challenge2binar.Data.MyData
import com.example.challenge2binar.R

class MyAdapter(private val context: Context, private val dataList: ArrayList<MyData>) : BaseAdapter() {

    private lateinit var image: ImageView
    private lateinit var name: TextView

    override fun getCount(): Int {
        return dataList.size
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        var convertView = LayoutInflater.from(context).inflate(R.layout.activity_gojek_grid, parent, false)
        image = convertView.findViewById(R.id.avatar)
        name = convertView.findViewById(R.id.name)

        var data: MyData = dataList[position]
        image.setImageResource(data.photoId)
        name.text = data.name
        return convertView
    }
}