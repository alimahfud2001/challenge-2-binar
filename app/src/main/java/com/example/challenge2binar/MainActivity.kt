package com.example.challenge2binar

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.challenge2binar.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.button1.setOnClickListener {openGojekView()}
        binding.button2.setOnClickListener {openTokopediaView()}
    }

    private fun openGojekView(){
        val intent = Intent(this, GojekActivity::class.java)
        startActivity(intent)
    }
    private fun openTokopediaView(){
        val intent = Intent(this, TokopediaActivity::class.java)
        startActivity(intent)
    }
}